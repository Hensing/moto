{ mkDerivation, base, di, moto, optparse-applicative, lib }:
mkDerivation {
  pname = "moto-example";
  version = "0.0.1";
  src = ./.;
  isLibrary = false;
  isExecutable = true;
  executableHaskellDepends = [ base di moto optparse-applicative ];
  description = "Example of how to use the moto migrations library";
  license = lib.licenses.publicDomain;
}
