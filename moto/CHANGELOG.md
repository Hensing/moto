# Version 0.0.4

* _COMPILER ASSISTED BREAKING CHANGE._ `registryConf_with` now takes a
  `Di` as extra argument.

* _BREAKING CHANGE._ `Moto.File` registry now now takes `/foo/bar` on the
  command line, rather than `file:///foo/bar`.

* Added `Moto.File.jsonStore`. See issue #11.

* Added `Moto.dummyStore`. See issue #3.

* Added `Moto.dummyBackup`. See issue #7.

* Added `--unsafe-commit` and `--unsafe-abort` switches to the `clean` CLI.
  These can be used to force the cleanup of a dirty migration registry without
  actually running any clean-up code, which can be useful in case of manual
  recovery. See issue #13.

* Made compatible with `di-df1` version `1.2`.

* Improved exception logging.


# Version 0.0.3

* Haddocks and Hackage choke on internal Cabal libraries, so we remove the
  internal `moto-internal` library.


# Version 0.0.2

* Added file missing from distribution.


# Version 0.0.1

* Initial version.
