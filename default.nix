# This file exports every derivation introduced by this repository.
{ nixpkgs ? import ./nixpkgs.nix }:
let pkgs = import ./pkgs.nix { inherit nixpkgs; };
in
pkgs.releaseTools.aggregate {
  name = "everything";
  constituents = [
    pkgs._here.ghc902.moto
    pkgs._here.ghc902.moto.doc
    pkgs._here.ghc902.moto-example
    pkgs._here.ghc902.moto-postgresql
    pkgs._here.ghc902.moto-postgresql.doc
    pkgs._here.ghc902._shell
  ];
}

