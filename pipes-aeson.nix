{ mkDerivation, aeson, attoparsec, base, bytestring, fetchgit, lib
, pipes, pipes-attoparsec, pipes-bytestring, pipes-parse
, transformers
}:
mkDerivation {
  pname = "pipes-aeson";
  version = "0.4.2";
  src = fetchgit {
    url = "https://github.com/hercules-ci/pipes-aeson.git";
    sha256 = "1gsz7sd0q5zrmwbrvk45va10pl5anfi0bcggi95131nd6i2rrmy7";
    rev = "ac735c9cd459c6ef51ba82325d1c55eb67cb7b2c";
    fetchSubmodules = true;
  };
  libraryHaskellDepends = [
    aeson attoparsec base bytestring pipes pipes-attoparsec
    pipes-bytestring pipes-parse transformers
  ];
  homepage = "https://github.com/k0001/pipes-aeson";
  description = "Encode and decode JSON streams using Aeson and Pipes";
  license = lib.licenses.bsd3;
}
