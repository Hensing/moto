serviceName:

# This expression can be used as a NixOS module.

# TODO: This hasn't been tested yet.

{ pkgs, config, lib, ... }:

with lib;
let
  motoServiceName = "moto-${serviceName}";
  cfg = config.services.${motoServiceName};
  workingDir = "/run/${motoServiceName}";
in {
  options = {
    services.${motoServiceName} = {
      enable = mkOption { default = false; type = types.bool; };
      registry = mkOption {
        type = types.string;
        description = ''
          URL to the <literal>moto</literal> migrations registry.

          Warning: This string is stored in the Nix store, so make sure you
          don't write sensitive information in it.
        '';
      };
      executable = mkOption {
        type = types.path;
        description = ''
          Path to the <literal>moto</literal> migrations executable.
        '';
      };
      extraArgs = mkOption {
        type = types.listOf types.string;
        default = "";
        description = ''
          Extra arguments to the <literal>moto</literal> migrations executable.

          Warning: This string is stored in the Nix store, so make sure you
          don't write sensitive information in it.
        '';
      };
    };
  };
  config = mkIf cfg.enable {
    system.activationScripts.${motoServiceName} = ''
      mkdir -p ${workingDir}
      chown -R ${motoServiceName}:${motoServiceName} ${workingDir}
    '';
    systemd.services.${motoServiceName} = {
      script = ''
        set -x
        ${cfg.executable} \
           ${escapeShellArgs cfg.extraArgs} \
           run --no-dry-run \
           --registry=${escapeShellArg cfg.registry}
        set +x
      '';
      wantedBy = [ "multi-user.target" ];
      after = [ "network.target" ];
      restartIfChanged = true;
      serviceConfig = {
        User = motoServiceName;
        Group = motoServiceName;
        Type = "oneshot";
        RemainAfterExit = true;
        WorkingDirectory = workingDir;
      };
    };
    users.extraGroups.${motoServiceName} = {
      gid = config.ids.gids.${motoServiceName};
    };
    users.extraUsers.${motoServiceName} = {
      uid = config.ids.uids.${motoServiceName};
      name = motoServiceName;
      group = motoServiceName;
    };
  };
}
