# haskell-updates 2022-03-21
builtins.fetchTarball {
  url = "https://github.com/NixOS/nixpkgs/archive/febc24b11ac5369734f81225dc8a200e7dc75319.tar.gz";
  sha256 = "0s3wcz0sn33jjb1wvsr95l0imrrljn2p97xfnhy7pq9p6m0k2di5";
}
