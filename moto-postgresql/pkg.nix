{ mkDerivation, base, bytestring, df1, di-df1, moto
, postgresql-simple, safe-exceptions, lib, text
}:
mkDerivation {
  pname = "moto-postgresql";
  version = "0.0.2";
  src = ./.;
  libraryHaskellDepends = [
    base bytestring df1 di-df1 moto postgresql-simple safe-exceptions
    text
  ];
  homepage = "https://gitlab.com/k0001/moto";
  description = "PostgreSQL-based migrations registry for moto";
  license = lib.licenses.asl20;
}
