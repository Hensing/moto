# Version 0.0.2

* Unexported `Moto.PostgreSQL.withRegistry`. See issue #19.

* Made compatible with `di-df1` version `1.2`.


# Version 0.0.1

* Initial version.
