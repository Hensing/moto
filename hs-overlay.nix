{ pkgs }:

# To be used as `packageSetConfig` for a Haskell pacakge set:
let
src_di =
  builtins.fetchGit {
    url = "https://github.com/k0001/di";
    rev = "adf1aa972546477b8848e8199a2a976646885a1f";
  };

inherit (pkgs.haskell.lib) dontCheck doJailbreak;

in
  self: super: {
    # https://github.com/k0001/pipes-aeson/pull/20
    # cabal2nix https://github.com/hercules-ci/pipes-aeson.git --revision refs/heads/ghc-9.0 >pipes-aeson.nix
    pipes-aeson = super.callPackage ./pipes-aeson.nix {};

    moto = super.callPackage ./moto/pkg.nix {};
    moto-example = super.callPackage ./moto-example/pkg.nix {};
    moto-postgresql = super.callPackage ./moto-postgresql/pkg.nix {};

    _shell = self.shellFor {
      withHoogle = true;
      nativeBuildInputs = [ self.cabal-install ];
      packages = p: [
        p.moto
        p.moto-example
        p.moto-postgresql
      ];
    };
  }
